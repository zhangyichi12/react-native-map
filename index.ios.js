/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    MapView
} from 'react-native';

class RN_Map extends Component {
    render() {
        return (
            <MapView style={ styles.map }>

            </MapView>
        );
    }
}

const styles = StyleSheet.create({
    map: {
        flex: 1
    }
});

AppRegistry.registerComponent('rn_map', () => RN_Map);
